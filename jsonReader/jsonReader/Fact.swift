//
//  Fact.swift
//
//
//  Created by Martina on 25.02.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

struct Fact: Decodable {
    let title: String
    let rows: [Row]
    
    private enum CodingKeys: String, CodingKey {
        case title = "title"
        case rows = "rows"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        rows = try container.decode([Row].self, forKey: .rows)
    }
}

struct Row: Decodable {
    let title: String
    let description: String
    let imageHref: String
    
    private enum CodingKeys: String, CodingKey {
        case title = "title"
        case description = "description"
        case imageHref = "imageHref"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        imageHref = try container.decodeIfPresent(String.self, forKey: .imageHref) ?? ""
    }
}
