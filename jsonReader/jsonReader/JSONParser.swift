//
//  JSONParser.swift
//  jsonReader
//
//  Created by Martina on 25.02.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

class JSONParser {
    var data: Data
    
    init(data: Data) {
        self.data = data
    }
    
    func getServices() -> [Service] {
        var result:[Service] = []
        do {
        result = try JSONDecoder().decode([Service].self, from: data)
        } catch let error {
            print(error)
        }
        return result
    }
    
    func getFacts() -> Fact? {
        var result: Fact?
        do {
            result = try JSONDecoder().decode(Fact.self, from: data)
        } catch let error {
            print(error)
        }
        return result
    }
}
