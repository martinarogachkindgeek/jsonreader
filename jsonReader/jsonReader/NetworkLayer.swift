//
//  NetworkLayer.swift
//  jsonReader
//
//  Created by Martina on 25.02.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

class NetworkLayer {
    var url: String?
    
    init(url: String?) {
        self.url = url
    }
    
    func getJSON(completion: @escaping (Data?) -> Void) {
        let session = URLSession(configuration: .default)
        if let urlString = url {
            if let url = URL(string: urlString) {
                let request = URLRequest(url: url)
                session.dataTask(with: request) { (data, responce, error) -> Void in
                    if let data = data {
                        let string = String(data: data, encoding: .isoLatin1) ?? ""
                        let utf8Data = Data(string.utf8)
                        completion(utf8Data)
                    }
                }.resume()
            }
        } else {
            completion(nil)
        }
    }
}
