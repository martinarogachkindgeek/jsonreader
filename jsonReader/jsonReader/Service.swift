//
//  jsonReader.swift
//  jsonReader
//
//  Created by Martina on 25.02.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import Foundation

struct Service: Decodable {
    let name: String
    let port: Int
    let versions: [Version]
    
    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case port = "port"
        case versions = "versions"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decode(String.self, forKey: .name)
        port = try container.decode(Int.self, forKey: .port)
        versions = try container.decode([Version].self, forKey: .versions)
    }
}

struct Version: Decodable {
    let consulServiceName: String
    let major: Int
    let minor: Int
    let patch: Int
    let url: String
    let status: String
    
    private enum CodingKeys: String, CodingKey {
        case consulServiceName = "consulServiceName"
        case major = "major"
        case minor = "minor"
        case patch = "patch"
        case url = "url"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        consulServiceName = try container.decode(String.self, forKey: .consulServiceName)
        major = try container.decode(Int.self, forKey: .major)
        minor = try container.decode(Int.self, forKey: .minor)
        patch = try container.decode(Int.self, forKey: .patch)
        url = try container.decode(String.self, forKey: .url)
        status = try container.decode(String.self, forKey: .status)
    }
}
