//
//  ViewController.swift
//  jsonReader
//
//  Created by Martina on 25.02.2020.
//  Copyright © 2020 MartinaRogach. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layer = NetworkLayer(url: "https://www.dropbox.com/s/83yxeqy36aaxhwx/Services.json?dl=1")
        layer.getJSON { data in
            if let data = data {
                let parser = JSONParser(data: data)
                let services = parser.getServices()
                for service in services {
                    print(service)
                }
            } else {
                print("error, no services data")
            }
        }
        let ano = NetworkLayer(url: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")
        ano.getJSON { data in
            if let data = data {
                let parcer = JSONParser(data: data)
                if let fact = parcer.getFacts() {
                    print(fact)
                }
            } else {
                print("error, no facts data")
            }
        }
    }
}

